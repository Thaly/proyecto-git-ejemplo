$(document).ready(function(){

	$('#frmDivIdTop').show();
	$('#frmRespIdTop').hide();

  $('#frmDivIdBottom').show();
  $('#frmRespIdBottom').hide();
  mostrarRegulatorios('regulatoriosMedusa', '/recargasonline');

  jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/i.test(value);
  }, "Letras únicamente");
     
  // Valida en KeyUp y Submit
  $("#formulario-padres-top").validate({
    rules: {
      // simple rule, converted to {required:true}
      numero_cel: {
        digits: true,
        required: true,
        minlength: 10,
        maxlength:10
      },
      // compound rule
      num_documento: {
        digits: true,
        required: true,
        minlength: 10,
        maxlength:10
      }
    },
    messages: {
      numero_cel: {
        digits: "Ingrese un número de celular válido",
        required: "Ingrese su número de celular",
        minlength: "Ingrese un número de celular válido",
        maxlength: "Ingrese un número de celular válido"
      },
      num_documento: {
        digits: "Ingrese un número de cédula válido",
        required: "Ingrese su número de cédula",
        minlength: "Ingrese un número de cédula válido",
        maxlength:"Ingrese un número de cédula válido"
      }
    },
    submitHandler: function(form, event) {
        event.preventDefault();
        var laddaBtn = Ladda.create(document.querySelector('#enviarbtnTop'));
        laddaBtn.start();
        laddaBtn.setProgress(0-1);
        var datos = {
				    inputs:{
								nombre: $('#nombre_formulario-padres-top').val(),
								tipo_documento: $('#tipo_documento_formulario-padres-top').val(),
								num_documento:  $('#num_documento_formulario-padres-top').val(),
								email_contacto:  $('#email_contacto_formulario-padres-top').val(),
								contactouno:  $('#numero_cel_formulario-padres-top').val(),
								numero_cel:  $('#numero_cel_formulario-padres-top').val(),
								temp_telf:  $('#numero_cel_formulario-padres-top').val(),
								equipo: $('#equipo_formulario-padres-top').val(),
								solicitud: $('#solicitud_formulario-padres-top').val(),
								ubicacion: $('#ubicacion_formulario-padres-top').val(),
								num_cel_pro: $('#numero_cel_formulario-padres-top').val(),
								tipo_contacto: $('#tipo_contacto_formulario-padres-top').val(),
								dia_contacto: $('#dia_contacto_formulario-padres-top').val(),
								hora_contacto: $('#hora_contacto_formulario-padres-top').val(),
								provincia: $('#provincia_formulario-padres-top').val(),
								agente: $('#agente_formulario-padres-top').val(),
								cod_promocional: $('#cod_promocional_formulario-padres-top').val(),
								utm_source: $('#utm_source_formulario-padres-top').val(),
								utm_medium: $('#utm_medium_formulario-padres-top').val(),
								utm_campaign: $('#utm_campaign_formulario-padres-top').val(),
								utm_content: $('#utm_content_formulario-padres-top').val(),
								form_llamada: $('#form_llamada_formulario-padres-top').val(),
								llamadaautomatica: $('#llamadaautomatica_formulario-padres-top').val(),
								campana: $('#campana_formulario-padres-top').val(),
								producto: $('#producto_formulario-padres-top').val(),
								stock: $('#stock_formulario-padres-top').val(),
								nip: $('#nip_formulario-padres-top').val(),
								tipo_linea: $('#tipo_linea_formulario-padres-top').val(),
								firma: $('#firma_formulario-padres-top').val(),
								chk_term: $('#chk_term_formulario-padres-top').val(),
								cc_derechos: $('#cc_derechos_formulario-padres-top').val(),
								op_actual: $('#op_actual_formulario-padres-top').val()
				    }
		    };

				$.ajax({
					type: 'POST',
					url: 'https://www.rigel-m.com/EnviarDatosFormulario',
					data: datos,
					dataType: "json",
					beforeSend: function(){
					  	laddaBtn.start();
					},
					success: function (data) {
					  laddaBtn.stop();
				  	$("#numero_cel_formulario-padres-top").val("");
          	$("#num_documento_formulario-padres-top").val("");
            $('#frmDivIdTop').hide();
						$('#frmRespIdTop').show();
						$('#frmRespIdTop').html(data.responseText);
						$('.d-flex').removeClass('d-flex');
					},
					error: function(data){
				  	laddaBtn.stop();
				  	$("#numero_cel_formulario-padres-top").val("");
            $("#num_documento_formulario-padres-top").val("");
            $('#frmDivIdTop').hide();
            $('#frmRespIdTop').show();
            $('#frmRespIdTop').html(data.responseText);
            $('.d-flex').removeClass('d-flex');
				  	console.log(data);
					}
				});
    }

  });

  // Valida en KeyUp y Submit
  $("#formulario-padres-bottom").validate({
    rules: {
      // simple rule, converted to {required:true}
      numero_cel: {
        digits: true,
        required: true,
        minlength: 10,
        maxlength:10
      },
      // compound rule
      num_documento: {
        digits: true,
        required: true,
        minlength: 10,
        maxlength:10
      }
    },
    messages: {
      numero_cel: {
        digits: "Ingrese un número de celular válido",
        required: "Ingrese su número de celular",
        minlength: "Ingrese un número de celular válido",
        maxlength: "Ingrese un número de celular válido"
      },
      num_documento: {
        digits: "Ingrese un número de cédula válido",
        required: "Ingrese su número de cédula",
        minlength: "Ingrese un número de cédula válido",
        maxlength:"Ingrese un número de cédula válido"
      }
    },
    submitHandler: function(form, event) {
        event.preventDefault();
        var laddaBtn = Ladda.create(document.querySelector('#enviarbtnBottom'));
        laddaBtn.start();
        laddaBtn.setProgress(0-1);
        var datos = {
            inputs:{
                nombre: $('#nombre_formulario-padres-bottom').val(),
                tipo_documento: $('#tipo_documento_formulario-padres-bottom').val(),
                num_documento:  $('#num_documento_formulario-padres-bottom').val(),
                email_contacto:  $('#email_contacto_formulario-padres-bottom').val(),
                contactouno:  $('#numero_cel_formulario-padres-bottom').val(),
                numero_cel:  $('#numero_cel_formulario-padres-bottom').val(),
                temp_telf:  $('#numero_cel_formulario-padres-bottom').val(),
                equipo: $('#equipo_formulario-padres-bottom').val(),
                solicitud: $('#solicitud_formulario-padres-bottom').val(),
                ubicacion: $('#ubicacion_formulario-padres-bottom').val(),
                num_cel_pro: $('#numero_cel_formulario-padres-bottom').val(),
                tipo_contacto: $('#tipo_contacto_formulario-padres-bottom').val(),
                dia_contacto: $('#dia_contacto_formulario-padres-bottom').val(),
                hora_contacto: $('#hora_contacto_formulario-padres-bottom').val(),
                provincia: $('#provincia_formulario-padres-bottom').val(),
                agente: $('#agente_formulario-padres-bottom').val(),
                cod_promocional: $('#cod_promocional_formulario-padres-bottom').val(),
                utm_source: $('#utm_source_formulario-padres-bottom').val(),
                utm_medium: $('#utm_medium_formulario-padres-bottom').val(),
                utm_campaign: $('#utm_campaign_formulario-padres-bottom').val(),
                utm_content: $('#utm_content_formulario-padres-bottom').val(),
                form_llamada: $('#form_llamada_formulario-padres-bottom').val(),
                llamadaautomatica: $('#llamadaautomatica_formulario-padres-bottom').val(),
                campana: $('#campana_formulario-padres-bottom').val(),
                producto: $('#producto_formulario-padres-bottom').val(),
                stock: $('#stock_formulario-padres-bottom').val(),
                nip: $('#nip_formulario-padres-bottom').val(),
                tipo_linea: $('#tipo_linea_formulario-padres-bottom').val(),
                firma: $('#firma_formulario-padres-bottom').val(),
                chk_term: $('#chk_term_formulario-padres-bottom').val(),
                cc_derechos: $('#cc_derechos_formulario-padres-bottom').val(),
                op_actual: $('#op_actual_formulario-padres-bottom').val()
            }
        };

        $.ajax({
          type: 'POST',
          url: 'https://www.rigel-m.com/EnviarDatosFormulario',
          data: datos,
          dataType: "json",
          beforeSend: function(){
              laddaBtn.start();
          },
          success: function (data) {
            laddaBtn.stop();
            $("#numero_cel_formulario-padres-bottom").val("");
            $("#num_documento_formulario-padres-bottom").val("");
            $('#frmDivIdBottom').hide();
            $('#frmRespIdBottom').show();
            $('#frmRespIdBottom').html(data.responseText);
            $('.d-flex').removeClass('d-flex');
          },
          error: function(data){
            laddaBtn.stop();
            $("#numero_cel_formulario-padres-bottom").val("");
            $("#num_documento_formulario-padres-bottom").val("");
            $('#frmDivIdBottom').hide();
            $('#frmRespIdBottom').show();
            $('#frmRespIdBottom').html(data.responseText);
            $('.d-flex').removeClass('d-flex');
            console.log(data);
          }
        });
    }

  });



});
